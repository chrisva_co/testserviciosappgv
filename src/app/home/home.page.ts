import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastController } from '@ionic/angular';
import swal from 'sweetalert2'
import AppConfig from '../pruebaServicios/AppConfig'
import AppPQRS from '../pruebaServicios/AppPQRS';
import ConsultaLiqui from '../pruebaServicios/ConsultaLiqui'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})


export class HomePage {

  private appConfig = { token: '' }
  private dominio = 'https://dev-gevir.paginaweb2.com'
  private mostrarIframe = false
  private urlPagoPrueba

  constructor(public toastCtrl: ToastController, public sanitizer: DomSanitizer) {
    this.getAppConfig(1, false)
  }

  setMostrarIframe(value){
    //Se le añade https por si no los trae
    const url = this.dominio.replace('http://','https://')+'/loader.php?lServicio=Usuarios&lTipo=consulta&m=1&hash=RMThaM2czTVRBME1ESTFOekkzZkRNNU9UQXdNak44UTBOOE16azJPRE16TVRSOE1RPT0='
    this.urlPagoPrueba = this.sanitizer.bypassSecurityTrustResourceUrl(url);
    console.log(this.urlPagoPrueba)
    this.mostrarIframe = value
  }
  
  async cambiarDominio(dominio){
    this.dominio = dominio

    const toast = this.toastCtrl.create({
      message: 'Cambiado a: '+dominio,
      duration: 3000,
      position: 'top'
    });
    (await toast).present();
  }

  async getAppConfig(numPrueba = 1, conAlert = true) {
    const appConfig = new AppConfig(this.dominio)
    let r = []

    switch (numPrueba) {
      case 1:
        r = await appConfig.probarPeticionExitosa()
        this.appConfig.token = r[1]?.data['token']
        break

      case 2:
        r = await appConfig.probarPeticionFallida2()
        break

      case 3:
        r = await appConfig.probarPeticionFallida3()
        break
    }

    if (r[1]) {
      if (conAlert) {
        swal.fire('Respuesta de appConfig (' + r[1].status + ')', JSON.stringify(r[1].data));
      }
    } else {
      if (conAlert) {
        if (r[1]?.response) {
          swal.fire('Error respuesta de appConfig (' + r[1].response.status + ')', JSON.stringify(r[1].response.data), 'warning');
        } else {
          swal.fire("Error desconocido. puede ser de cors o de falta de conexión")
        }
      }
      console.log(r[1]?.response)
    }
  }

  async getConsultaLiqui(numPrueba = 1) {
    const consultaLiqui = new ConsultaLiqui(this.dominio, this.appConfig.token)
    let r = []

    switch (numPrueba) {
      case 1:
        const result = await swal.fire({
          title: 'Complete datos',
          html:
            'Tipo de documento: <b>CC</b> <br/> <br/>' +
            'Número de documento: <input id="swal-input2" class="swal2-input" value="1112223">'+
            'Número de liquidación: <input id="swal-input3" class="swal2-input" value="">',
            
          showCancelButton: true,
          confirmButtonText: 'Consultar',
          showLoaderOnConfirm: true,
          preConfirm: () => {
            let cc = 'CC'
            let cedula = (<HTMLInputElement>document.getElementById('swal-input2')).value
            let numLiquidacion = (<HTMLInputElement>document.getElementById('swal-input3')).value

            return consultaLiqui.probarPeticionExitosa(cc, cedula, numLiquidacion)
          },
          allowOutsideClick: () => !swal.isLoading()
        })

        if (result.isConfirmed) {
          r = result.value
        }else{
          return ; //si le dio cancelar
        }
        break

      case 2:
        r = await consultaLiqui.probarPeticionFallida2()
        break

      case 3:
        r = await consultaLiqui.probarPeticionFallida3()
        break
    }

    if (r[1]) {
      console.log(r[1].data)
      swal.fire('Respuesta de appConsultaLiqui (' + r[1].status + ')', JSON.stringify(r[1].data));
    } else {
      if (r[1]?.response) {
        swal.fire('Error respuesta de appConsultaLiqui (' + r[1].response.status + ')', JSON.stringify(r[1].response.data), 'warning');
      } else {
        swal.fire("Error desconocido. puede ser de cors o de falta de conexión")
      }
      console.log(r[1]?.response)
    }
  }



  async getAppPQRS(numPrueba = 1) {
    const appPQRS = new AppPQRS(this.dominio, this.appConfig.token)
    let r = []

    switch (numPrueba) {
      case 1:
        r = await appPQRS.probarPeticionExitosa()
        break

      case 2:
        r = await appPQRS.probarPeticionFallida2()
        break

      case 3:
        r = await appPQRS.probarPeticionFallida3()
        break

      case 4:
        r = await appPQRS.probarPeticionFallida4()
        break
    }

    if (r[1]) {
      swal.fire('Respuesta de appConsultaLiqui (' + r[1].status + ')', JSON.stringify(r[1].data));
    } else {
      if (r[1]?.response) {
        swal.fire('Error respuesta de appConsultaLiqui (' + r[1].response.status + ')', JSON.stringify(r[1].response.data), 'warning');
      } else {
        swal.fire("Error desconocido. puede ser de cors o de falta de conexión")
      }
      console.log(r[1]?.response)
    }
  }

}


