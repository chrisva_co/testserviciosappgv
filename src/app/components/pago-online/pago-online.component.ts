import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pago-online',
  templateUrl: './pago-online.component.html',
  styleUrls: ['./pago-online.component.scss'],
})
export class PagoOnlineComponent implements OnInit {

  @Input() urlPagoPrueba: any;
  
  @Output() someEvent = new EventEmitter<boolean>();

  callParent(): void {
    this.someEvent.next(false);
  }
  
  constructor() { }

  ngOnInit() {
  }
  
}
