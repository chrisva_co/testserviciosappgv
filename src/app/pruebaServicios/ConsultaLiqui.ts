import axios from 'axios'


export default class ConsultaLiqui {

    private dominio = ''
    private token = ''

    constructor(dominio, token) {
        this.dominio = dominio
        this.token = token
    }

    /**
     * Prueba exitosa del servicio appconsultaliqui
     */
    async probarPeticionExitosa(tipoDoc: string, cedula: string, numLiquidacion='') {
        const headers = {
            "Content-Type": "application/json",
            "Token": this.token
        }
        const data = {
            "tipoIdentificacion": tipoDoc,
            "numeroIdentificacion": cedula,
            "numeroLiquidacion": numLiquidacion  //Opional
          }
        try {
            const r = await axios.post(this.dominio + '/loader.php?lServicio=Usuarios&lTipo=wsm&lFuncion=appConsultaLiqui', 
            data, { headers: headers })
            return [true, r]
        } catch (e) {
            return [false, e.response]
        }
    }

    /**
     * Prueba de error cuando el error es del contenido
     */
    async probarPeticionFallida2() {
        const headers = {
            "Content-Type": "application/json",
            "Token": this.token
        }
        const data = {
            "tipoIdentificacion": "CC",
            "numeroIdentificacion": "",
            //"numeroLiquidacion": "10398088621"  //Opional
          }
        try {
            const r = await axios.post(this.dominio + '/loader.php?lServicio=Usuarios&lTipo=wsm&lFuncion=appConsultaLiqui', 
            data, { headers: headers })
            return [true, r]
        } catch (e) {
            return [false, e.response]
        }
    }

    /**
     * Prueba de error cuando el error es de token
     */
    async probarPeticionFallida3() {
        const headers = {
            "Content-Type": "application/json",
            "Token": "Sin token"
        }
        const data = {
            "tipoIdentificacion": "CC",
            "numeroIdentificacion": "",
            //"numeroLiquidacion": "10398088621"  //Opional
          }
        try {
            const r = await axios.post(this.dominio + '/loader.php?lServicio=Usuarios&lTipo=wsm&lFuncion=appConsultaLiqui', 
            data, { headers: headers })
            return [true, r]
        } catch (e) {
            return [false, e.response]
        }
    }


    /**
     * Prueba de error cuando el error es de la cabecera (headers)
     */
    async probarPeticionFallida4() {
        const headers = {
            "CabezeraFalsa": "application/json",
        }
        const data = {
            "tipoIdentificacion": "CC",
            "numeroIdentificacion": "",
            //"numeroLiquidacion": "10398088621"  //Opional
          }
        try {
            const r = await axios.post(this.dominio + '/loader.php?lServicio=Usuarios&lTipo=wsm&lFuncion=appConsultaLiqui', 
            data, { headers: headers })
            return [true, r]
        } catch (e) {
            return [false, e.response]
        }
    }

}

