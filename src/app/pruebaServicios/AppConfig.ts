import axios from 'axios'

export var token = ''

export default class AppConfig {

    private dominio = ''

    constructor(dominio) {
        this.dominio = dominio
    }

    async probarPeticionExitosa() {
        const headers = {
            "Content-Type": "application/json"
        }
        const data = {
            "codigoAutorizacion": btoa('ne' + btoa('xu' + btoa('ra' + btoa("gx7_" + fechaFormateada()))))
        }
        try {
            const r = await axios.post(this.dominio + '/loader.php?lServicio=Usuarios&lTipo=wsm&lFuncion=appConfig', data, { headers: headers })
            return [true, r]
        } catch (e) {
            return [false, e.response]
        }
    }

    /**
     * Prueba de error cuando el error es del contenido
     */
    async probarPeticionFallida2() {
        const headers = {
            "Content-Type": "application/json"
        }
        const data = {
            "codigoAutorizacion": "Autorizacion INVALIDA"
        }
        try {
            const r = await axios.post(this.dominio + '/loader.php?lServicio=Usuarios&lTipo=wsm&lFuncion=appConfig', data, { headers: headers })
            return [true, r]
        } catch (e) {
            return [false, e.response]
        }
    }

    /**
     * Prueba de error cuando el error es de la cabecera (headers)
     */
    async probarPeticionFallida3() {
        const headers = {
            "Content-Type-cors": "otra cabezera"
        }
        const data = {
            "codigoAutorizacion": btoa('ne' + btoa('xu' + btoa('ra' + btoa("gx7_" + fechaFormateada()))))
        }
        try {
            const r = await axios.post(this.dominio + '/loader.php?lServicio=Usuarios&lTipo=wsm&lFuncion=appConfig', data, { headers: headers })
            return [true, r]
        } catch (e) {
            return [false, e.response]
        }
    }

}



/**
 * Muestra la fecha en formato dd-mm-yyyy
 */
function fechaFormateada() {
    let date = new Date()

    let day = (date.getDate()).toString()
    let month = (date.getMonth() + 1).toString()
    let year = (date.getFullYear()).toString()
    let res = ''
    if (parseInt(day) < 10) {
        day = `0${day}`;
    } 
    if (parseInt(month) < 10) {
        month = `0${month}`;
    } 
    res = `${day}-${month}-${year}`
    return res
}