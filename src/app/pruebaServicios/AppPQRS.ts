import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios'


export default class ConsultaLiqui {

    private dominio = ''
    private token = ''

    constructor(dominio, token) {
        this.dominio = dominio
        this.token = token
    }

    /**
     * Prueba exitosa del servicio appconsultaliqui
     */
    async probarPeticionExitosa() {
        const headers = {
            "Content-Type": "application/json",
            "Token": this.token
        }
        const data = {
            "categoria": "1",
            "nombre": "Jhon Doe",
            "email": "un_correo@pagina123.com",
            "comentario": "Comentario de prueba"
          }
        try {
            const r = await axios.post(this.dominio + '/loader.php?lServicio=Usuarios&lTipo=wsm&lFuncion=appPQRS', 
            data, { headers: headers })
            return [true, r.data]
        } catch (e) {
            return [false, e.response]
        }
    }

    /**
     * Prueba de error cuando el error es del contenido
     */
    async probarPeticionFallida2() {
        const headers = {
            "Content-Type": "application/json",
            "Token": this.token
        }
        const data = {
            "categoria": "1",
            "nombre": "Jhon Doe",
            "email": "un_correoFalso",
            "comentario": "Comentario de prueba"
          }
        try {
            const r = await axios.post(this.dominio + '/loader.php?lServicio=Usuarios&lTipo=wsm&lFuncion=appPQRS', 
            data, { headers: headers })
            return [true, r]
        } catch (e) {
            return [false, e.response]
        }
    }

    /**
     * Prueba de error cuando el error es de token
     */
    async probarPeticionFallida3() {
        const headers = {
            "Content-Type": "application/json",
            "Token": "Sin token"
        }
        const data = {
            "categoria": "1",
            "nombre": "Jhon Doe",
            "email": "un_correo@pagina123.com",
            "comentario": "Comentario de prueba"
          }
        try {
            const r = await axios.post(this.dominio + '/loader.php?lServicio=Usuarios&lTipo=wsm&lFuncion=appPQRS', 
            data, { headers: headers })
            return [true, r]
        } catch (e) {
            return [false, e.response]
        }
    }

    /**
     * Prueba de error cuando el error es de la cabecera (headers)
     */
    async probarPeticionFallida4() {
        const headers = {
            "cabecera falsa": "application/json",
        }
        const data = {
            "categoria": "1",
            "nombre": "Jhon Doe",
            "email": "un_correo@pagina123.com",
            "comentario": "Comentario de prueba"
          }
        try {
            const r = await axios.post(this.dominio + '/loader.php?lServicio=Usuarios&lTipo=wsm&lFuncion=appPQRS', 
            data, { headers: headers })
            return [true, r]
        } catch (e) {
            return [false, e.response]
        }
    }

}

